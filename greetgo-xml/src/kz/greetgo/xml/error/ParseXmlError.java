package kz.greetgo.xml.error;

public class ParseXmlError extends RuntimeException {
  public ParseXmlError(Exception e) {
    super(e);
  }

  public ParseXmlError() {}
}
