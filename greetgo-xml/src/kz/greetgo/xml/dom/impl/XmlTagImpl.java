package kz.greetgo.xml.dom.impl;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import kz.greetgo.xml.dom.XmlTag;
import kz.greetgo.xml.error.ParseXmlError;
import kz.greetgo.xml.error.ParseXmlNull;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlTagImpl implements XmlTag {

  private final Document document;
  private final Element  tag;

  private final Map<String, String> namespaceUriToPrefix = new HashMap<>();
  private final Map<String, String> namespacePrefixToUri = new HashMap<>();

  public XmlTagImpl(@NonNull Document document, @NonNull Element tag) {
    this.document = document;
    this.tag      = tag;
  }

  public XmlTagImpl(@NonNull XmlTagImpl bro, @NonNull Element tag) {
    this.document = bro.document;
    this.tag      = tag;
    copyNamespaceAliasesFrom(bro);
  }

  @Override
  public Map<String, String> namespaceUriToPrefix() {
    return Map.copyOf(namespaceUriToPrefix);
  }

  @SneakyThrows
  private static DocumentBuilder newDocumentBuilder() {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true);

    return factory.newDocumentBuilder();
  }

  @SneakyThrows
  public static @NonNull XmlTagImpl parseXmlText(String xmlText) {
    if (xmlText == null) {
      throw new ParseXmlNull();
    }

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true);

    DocumentBuilder builder = factory.newDocumentBuilder();

    {
      StringReader stringReader = new StringReader(xmlText);
      InputSource  inputSource  = new InputSource(stringReader);

      try {
        final Document document = builder.parse(inputSource);
        return new XmlTagImpl(document, document.getDocumentElement());
      } catch (Exception e) {
        throw new ParseXmlError(e);
      }
    }
  }

  public static @NonNull XmlTagImpl root(String namespaceUri, @NonNull String qName) {
    final DocumentBuilder builder  = newDocumentBuilder();
    final Document        document = builder.newDocument();

    final int idx = qName.indexOf(":");
    if (idx < 0) {
      final Element rootElement = document.createElement(qName);
      document.appendChild(rootElement);
      return new XmlTagImpl(document, rootElement);
    }

    {
      final Element rootElement = document.createElementNS(namespaceUri, qName);
      document.appendChild(rootElement);
      final XmlTagImpl ret = new XmlTagImpl(document, rootElement);
      ret.addNamespacePrefix(namespaceUri, qName.substring(0, idx));
      return ret;
    }
  }

  @Override
  public String outerXml() {
    return convertToStr(false);
  }

  @Override
  public String outerXmlPretty() {
    return convertToStr(true);
  }

  @SneakyThrows
  private String convertToStr(boolean pretty) {
    TransformerFactory transformerFactory = TransformerFactory.newInstance();

//    if (pretty) {
//      transformerFactory.setAttribute("indent-number", 2);
//    }

    Transformer  transformer = transformerFactory.newTransformer();
    DOMSource    domSource   = new DOMSource(tag);
    StringWriter outString   = new StringWriter();
    StreamResult result      = new StreamResult(outString);

    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");

    if (pretty) {
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    }

    transformer.transform(domSource, result);

    return outString.toString();
  }

  @Override
  public void addNamespacePrefix(String namespaceUri, String alias) {
    namespaceUriToPrefix.put(namespaceUri, alias);
    namespacePrefixToUri.put(alias, namespaceUri);
  }

  @Override
  public XmlTagImpl gotoTag(String qNamePath) {
    if (qNamePath == null) {
      return this;
    }

    Element current = tag;

    if (qNamePath.startsWith("/")) {
      current   = document.getDocumentElement();
      qNamePath = qNamePath.substring(1);
    }

    for (final String qName : qNamePath.split("/")) {
      current = selectChildOrCreate(current, qName);
    }

    return new XmlTagImpl(this, current);
  }

  private void copyNamespaceAliasesFrom(@NonNull XmlTagImpl other) {
    namespacePrefixToUri.putAll(other.namespacePrefixToUri);
    namespaceUriToPrefix.putAll(other.namespaceUriToPrefix);
  }

  @ToString
  @RequiredArgsConstructor
  private class TagName {
    final String namespaceUri;
    final String localName;

    boolean eq(Element element) {
      return Objects.equals(namespaceUri, element.getNamespaceURI())
             && Objects.equals(localName, element.getLocalName());
    }

    Element create() {
      if (namespaceUri == null) {
        return document.createElement(localName);
      }
      final String alias = namespaceUriToPrefix.get(namespaceUri);
      if (alias != null) {
        return document.createElementNS(namespaceUri, alias + ":" + localName);
      }
      return document.createElementNS(namespaceUri, localName);
    }
  }

  private TagName parseQName(@NonNull String qName) {
    final int idx = qName.indexOf(":");
    if (idx < 0) {
      return new TagName(null, qName);
    }

    final String namespaceAlias = qName.substring(0, idx);
    final String localName      = qName.substring(idx + 1);

    final String uri = namespacePrefixToUri.get(namespaceAlias);
    if (uri != null) {
      return new TagName(uri, localName);
    }

    final String namespaceUri = document.lookupNamespaceURI(namespaceAlias);
    if (namespaceUri != null) {
      return new TagName(namespaceUri, localName);
    }

    return new TagName(null, localName);
  }

  private @NonNull Element selectChildOrCreate(@NonNull Element current, @NonNull String qName) {

    final TagName childName = parseQName(qName);

    final NodeList childNodes = current.getChildNodes();
    final int      length     = childNodes.getLength();
    for (int i = 0; i < length; i++) {
      final Node item = childNodes.item(i);
      if (item instanceof Element child) {
        if (childName.eq(child)) {
          return child;
        }
      }
    }

    {
      final Element child = childName.create();
      current.appendChild(child);
      return child;
    }
  }

  @Override
  public String getInnerText() {
    return tag.getTextContent();
  }

  @Override
  public XmlTagImpl setInnerText(String innerText) {
    tag.setTextContent(innerText);
    return this;
  }

  @Override
  public XmlTagImpl append(@NonNull XmlTag anotherTag) {
    if (!(anotherTag instanceof XmlTagImpl x)) {
      throw new RuntimeException("1GBO6ab1ot :: Illegal argument class");
    }
    tag.appendChild(document.importNode(x.tag, true));
    return this;
  }

  @Override
  public XmlTagImpl appendNewTag(@NonNull String tagName) {
    final Element newElement = findNamespace(tagName).map(x -> document.createElementNS(x.namespaceUri, tagName))
                                                     .orElseGet(() -> document.createElement(tagName));

    tag.appendChild(newElement);

    return new XmlTagImpl(this, newElement);
  }

  @RequiredArgsConstructor
  private static class NsName {
    final @NonNull String namespaceUri;
    final @NonNull String localName;
    final @NonNull String prefix;
  }

  private @NonNull Optional<NsName> findNamespace(@NonNull String tagName) {
    final int idx = tagName.indexOf(":");
    if (idx < 0) {
      return Optional.empty();
    }

    final String prefix    = tagName.substring(0, idx);
    final String localName = tagName.substring(idx + 1);

    {
      final String namespaceUri = namespacePrefixToUri.get(prefix);
      if (namespaceUri != null) {
        return Optional.of(new NsName(namespaceUri, localName, prefix));
      }
    }

    {
      final String namespaceURI = tag.lookupNamespaceURI(prefix);
      if (namespaceURI != null) {
        return Optional.of(new NsName(namespaceURI, localName, prefix));
      }
    }

    throw new RuntimeException("4512QLc1sg :: не найдено пространство имён для " + prefix);
  }

  @Override
  public XmlTagImpl setAttribute(@NonNull String attrName, String attrValue) {
    findNamespace(attrName)
      .ifPresentOrElse(x -> tag.setAttributeNS(x.namespaceUri, attrName, attrValue),
                       () -> tag.setAttribute(attrName, attrValue));
    return this;
  }

  @Override
  public String getAttribute(@NonNull String attrName) {
    final NsName nsName = findNamespace(attrName).orElse(null);
    if (nsName != null) {
      return tag.getAttributeNS(nsName.namespaceUri, nsName.localName);
    }
    return tag.getAttribute(attrName);
  }

  @Override
  public XmlTag removeAttribute(String attrName) {
    findNamespace(attrName)
      .ifPresentOrElse(x -> tag.removeAttributeNS(x.namespaceUri, x.localName),
                       () -> tag.removeAttribute(attrName));
    document.normalizeDocument();
    return this;
  }

  @Override
  public List<XmlTag> children() {
    final NodeList children = tag.getChildNodes();
    List<XmlTag>   ret      = new ArrayList<>();
    for (int i = 0, length = children.getLength(); i < length; i++) {
      final Node item = children.item(i);
      if (item instanceof Element e) {
        ret.add(new XmlTagImpl(this, e));
      }
    }
    return ret;
  }

  @Override
  public String tagName() {

    final String tagName = tag.getTagName();

    final int idx = tagName.indexOf(":");
    if (idx < 0) {
      return tagName;
    }

    String prefix     = tagName.substring(0, idx);
    String simpleName = tagName.substring(idx + 1);

    final String namespaceURI = tag.lookupNamespaceURI(prefix);

    final String myPrefix = namespaceUriToPrefix.get(namespaceURI);

    String existsPrefix = myPrefix != null ? myPrefix : prefix;

    return existsPrefix + ":" + simpleName;
  }
}
