package kz.greetgo.xml.dom;

import java.util.List;
import java.util.Map;
import kz.greetgo.xml.dom.impl.XmlTagImpl;
import lombok.NonNull;

public interface XmlTag {

  static @NonNull XmlTag parseXmlText(String xmlText) {
    return XmlTagImpl.parseXmlText(xmlText);
  }

  static @NonNull XmlTag root(String namespaceUri, @NonNull String qName) {
    return XmlTagImpl.root(namespaceUri, qName);
  }

  String outerXml();

  String outerXmlPretty();

  void addNamespacePrefix(String namespaceUri, String alias);

  Map<String, String> namespaceUriToPrefix();

  XmlTag gotoTag(String qNamePath);

  String getInnerText();

  XmlTag setInnerText(String innerText);

  XmlTag append(@NonNull XmlTag anotherTag);

  XmlTag appendNewTag(@NonNull String tagName);

  XmlTag setAttribute(@NonNull String attrName, String attrValue);

  String getAttribute(@NonNull String attrName);

  List<XmlTag> children();

  XmlTag removeAttribute(String attrName);

  String tagName();
}
