package kz.greetgo.xml.sax;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import lombok.NonNull;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public abstract class SaxHandler extends DefaultHandler {

  protected static <Handler extends SaxHandler> @NonNull Handler parse(@NonNull Handler handler, @NonNull String xmlText) {

    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setNamespaceAware(true);
      SAXParser   saxParser   = factory.newSAXParser();
      InputSource inputSource = new InputSource(new StringReader(xmlText));
      saxParser.parse(inputSource, handler);

    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new RuntimeException("yUCn9AP7Qs :: SAX parse error", e);
    }

    return handler;
  }

  private final Map<String, String> namespaceUriToUserAlias = new HashMap<>();


  private final List<String> pathList = new ArrayList<>();

  protected final @NonNull String tagPath() {
    return "/" + String.join("/", pathList);
  }

  private StringBuilder text = null;

  protected final String text() {
    final StringBuilder s = text;
    return s == null ? "" : s.toString();
  }

  // override it
  protected Map<String, String> collectNamespacesToUserAlias() {
    return Map.of();
  }

  @Override
  public final void startDocument() throws SAXException {
    namespaceUriToUserAlias.putAll(collectNamespacesToUserAlias());
    try {
      startDoc();
    } catch (RuntimeException | SAXException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @SuppressWarnings("RedundantThrows")
  protected void startDoc() throws Exception {
    // override it
  }

  private String extractName(String uri, String localName, String qName) {
    if (uri == null || uri.isBlank()) {
      return localName;
    }
    final String userAlias = namespaceUriToUserAlias.get(uri);
    if (userAlias == null) {
      return qName;
    }
    return userAlias + ":" + localName;
  }

  @Override
  public final void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
    pathList.add(extractName(uri, localName, qName));
    text = null;

    try {
      startTag(attributes);
    } catch (SAXException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  protected abstract void startTag(Attributes attributes) throws Exception;

  @Override
  public final void characters(char[] ch, int start, int length) {

    StringBuilder s = text;

    (s != null ? s : (text = new StringBuilder())).append(ch, start, length);

  }

  @Override
  public final void endElement(String uri, String localName, String qName) throws SAXException {
    try {
      endTag();
    } catch (SAXException e) {
      throw e;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    text = null;

    pathList.remove(pathList.size() - 1);
  }

  protected abstract void endTag() throws Exception;

}
