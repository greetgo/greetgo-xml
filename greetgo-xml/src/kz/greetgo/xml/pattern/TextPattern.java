package kz.greetgo.xml.pattern;

import java.util.HashMap;
import java.util.Map;
import kz.greetgo.xml.dom.XmlTag;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class TextPattern {
  public final String open, close;

  private final Map<String, String> varValues = new HashMap<>();

  public static TextPattern create(String open, String close) {
    return new TextPattern(open, close);
  }

  public static TextPattern of() {
    return create("{{", "}}");
  }

  public TextPattern setVarValue(String varName, String varValue) {
    varValues.put(varName, varValue);
    return this;
  }

  public String convert(String source) {
    if (source == null) {
      return null;
    }

    StringBuilder sb = new StringBuilder();

    int       index  = 0;
    final int length = source.length();

    while (index < length) {

      final int i1 = source.indexOf(open, index);
      if (i1 < 0) {
        break;
      }

      final int i2 = source.indexOf(close, i1 + open.length());
      if (i2 < 0) {
        break;
      }

      sb.append(source, index, i1);

      final String varName = source.substring(i1 + open.length(), i2);

      if (varName.length() > 0) {
        final String varValue = varValues.get(varName);
        if (varValue != null) {
          sb.append(varValue);
        }
      }

      index = i2 + close.length();
    }

    if (index < length) {
      sb.append(source, index, length);
    }

    return sb.toString();
  }

  public XmlTag convertToXml(String xmlText) {
    return XmlTag.parseXmlText(convert(xmlText));
  }
}
