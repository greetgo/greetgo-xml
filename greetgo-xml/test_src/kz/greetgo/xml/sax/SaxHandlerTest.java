package kz.greetgo.xml.sax;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.testng.annotations.Test;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import static org.assertj.core.api.Assertions.assertThat;

public class SaxHandlerTest {

  static class TestHandler extends DefaultHandler {
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
      System.out.println("GwhL569f1U :: uri = " + uri + ", localName = " + localName + ", qName = " + qName);
    }
  }

  @Test
  public void probeNamespaces() throws Exception {

    String xml = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="http://MyBpm">
                  <ns2:name>west</ns2:name>
                  <ns2:surname>kanye</ns2:surname>
                  <ns2:patronymic></ns2:patronymic>
                  <ns2:iin>7984567981</ns2:iin>
                  <stone>Stone</stone>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    SAXParserFactory factory = SAXParserFactory.newInstance();
    factory.setNamespaceAware(true);

    SAXParser saxParser = factory.newSAXParser();

    saxParser.parse(new InputSource(new StringReader(xml)), new TestHandler());
  }

  public static class TestSaxHandler extends SaxHandler {
    public final List<String> tagPaths  = new ArrayList<>();
    public final List<String> tagValues = new ArrayList<>();

    @Override
    protected void startTag(Attributes attributes) {
      tagPaths.add(tagPath());
      System.out.println("ZD0Hk78cTl :: Start tag: " + tagPath());
    }

    @Override
    protected void endTag() {
      if (!text().isBlank()) {
        tagValues.add(tagPath() + " -> " + text().trim());
      }
    }

    @Override
    protected Map<String, String> collectNamespacesToUserAlias() {
      return Map.of(
        "http://schemas.xmlsoap.org/soap/envelope/", "o",
        "http://MyBpm", "m",
        "https://core.mybpm.kz/create-client-header/", "x"
      );
    }
  }

  @Test
  public void probeSimpleTag() {
    //language=XML
    String xml = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header>
            <ns1:CreateClientHeader xmlns:ns1="https://core.mybpm.kz/create-client-header/">
              <ns1:Status>Это статус</ns1:Status>
              <ns1:Result>Это результат</ns1:Result>
            </ns1:CreateClientHeader>
          </SOAP-ENV:Header>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="http://MyBpm">
                  <ns2:name>west</ns2:name>
                  <ns2:surname>kanye</ns2:surname>
                  <sky>Sky Walker</sky>
                  <stone>Stone</stone>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    final TestSaxHandler handler = TestSaxHandler.parse(new TestSaxHandler(), xml);

    {
      int i = 0;
      for (final String tagValue : handler.tagValues) {
        System.out.println("assertThat(handler.tagValues.get(" + i++ + ")).isEqualTo(\"" + tagValue + "\");");
      }
    }

    assertThat(handler.tagValues.get(0)).isEqualTo("/o:Envelope/o:Header/x:CreateClientHeader/x:Status -> Это статус");
    assertThat(handler.tagValues.get(1)).isEqualTo("/o:Envelope/o:Header/x:CreateClientHeader/x:Result -> Это результат");
    assertThat(handler.tagValues.get(2)).isEqualTo("/o:Envelope/o:Body/m:CreateClient/m:name -> west");
    assertThat(handler.tagValues.get(3)).isEqualTo("/o:Envelope/o:Body/m:CreateClient/m:surname -> kanye");
    assertThat(handler.tagValues.get(4)).isEqualTo("/o:Envelope/o:Body/m:CreateClient/sky -> Sky Walker");
    assertThat(handler.tagValues.get(5)).isEqualTo("/o:Envelope/o:Body/m:CreateClient/stone -> Stone");

  }

}