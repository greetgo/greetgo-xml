package kz.greetgo.xml.pattern;

import kz.greetgo.xml.dom.XmlTag;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TextPatternTest {

  @Test
  public void convert__mainSequence() {

    final TextPattern textPattern = TextPattern.of();

    textPattern.setVarValue("SIN", "Diamond").setVarValue("COS", "Factory");

    String source   = "Hello {{SIN}} of {{COS}}. And status {{COS}} was single {{SIN}}.";
    String expected = "Hello Diamond of Factory. And status Factory was single Diamond.";

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convert__emptyPattern() {

    final TextPattern textPattern = TextPattern.of();

    String source   = "Begin {{}} end";
    String expected = "Begin  end";

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convert__patternAtTheEnd() {

    final TextPattern textPattern = TextPattern.of();

    String source   = "Begin {{b}}";
    String expected = "Begin BUILD";

    textPattern.setVarValue("b", "BUILD");

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convert__patternInTheBeginning() {

    final TextPattern textPattern = TextPattern.of();

    String source   = "{{fi}} Status";
    String expected = "The First Status";

    textPattern.setVarValue("fi", "The First");

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convert__onlyPattern() {

    final TextPattern textPattern = TextPattern.of();

    String source   = "{{STATUS}}";
    String expected = "The First Stone";

    textPattern.setVarValue("STATUS", "The First Stone");

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convert__noPattern() {

    final TextPattern textPattern = TextPattern.of();

    String source   = "Simple text";
    String expected = "Simple text";

    //
    //
    final String actual = textPattern.convert(source);
    //
    //

    assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void convertToXml() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="{{STONE}}" status="{{STATUS}}">{{NAME}}</ns2:name>
                  <ns2:surname ns2:Stone="{{STONE}}" status="{{STATUS}}">{{SURNAME}}</ns2:surname>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    final TextPattern textPattern = TextPattern.of();

    textPattern.setVarValue("STONE", "A-Metis-t").setVarValue("STATUS", "OK");
    textPattern.setVarValue("SURNAME", "Пушкин").setVarValue("NAME", "Станислав");

    final XmlTag xmlTag = textPattern.convertToXml(xmlText);

    System.out.println("4N5y63aCxA :: xmlTag = \n" + xmlTag.outerXml());

    //language=XML
    String expectedText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="A-Metis-t" status="OK">Станислав</ns2:name>
                  <ns2:surname ns2:Stone="A-Metis-t" status="OK">Пушкин</ns2:surname>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedText.replaceAll("\\s+", ""));
  }
}