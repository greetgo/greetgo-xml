package kz.greetgo.xml.dom;

import java.util.List;
import java.util.stream.Collectors;
import kz.greetgo.xml.error.ParseXmlError;
import kz.greetgo.xml.error.ParseXmlNull;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class XmlTagTest {

  @Test
  public void parseXmlText__outerXml() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>west</ns2:name>
                  <ns2:surname>kanye</ns2:surname>
                  <ns2:iin>098234876123</ns2:iin>
                  <ns2:tel>+7 707 123 45 67</ns2:tel>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    //
    //
    final XmlTag root = XmlTag.parseXmlText(xmlText);
    //
    //

    //
    //
    final String outerXml = root.outerXml();
    //
    //

    assertThat(outerXml.replaceAll("\\s", "")).isEqualTo(xmlText.replaceAll("\\s", ""));
  }

  @Test
  public void gotoTag__gotoExistSubElement() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>west</ns2:name>
                  <ns2:surname>kanye</ns2:surname>
                  <ns2:iin>098234876123</ns2:iin>
                  <ns2:tel>+7 707 123 45 67</ns2:tel>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    final XmlTag root = XmlTag.parseXmlText(xmlText);

    root.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    root.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "c");

    //
    //
    final XmlTag subTag = root.gotoTag("soap:Body/c:CreateClient/c:name");
    //
    //

    //
    //
    final String tagText = subTag.getInnerText();
    //
    //

    assertThat(tagText).isEqualTo("west");

    //language=XML
    final String expectedXml = """
      <?xml version="1.0" encoding="UTF-8"?>
      <ns2:name xmlns:ns2="https://core.mybpm.kz/api-services/bo/">west</ns2:name>
      """;

    assertThat(subTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXml.replaceAll("\\s+", ""));
  }

  @Test
  public void root__gotoTag__setInnerText() {

    //
    //
    final XmlTag root = XmlTag.root("http://schemas.xmlsoap.org/soap/envelope/", "soap:Top");
    //
    //

    root.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "c");

    //
    //
    final XmlTag subTag = root.gotoTag("soap:Header/c:Client/c:Name");
    //
    //

    assertThat(subTag.namespaceUriToPrefix()).containsEntry("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    assertThat(subTag.namespaceUriToPrefix()).containsEntry("https://core.mybpm.kz/api-services/bo/", "c");

    //
    //
    subTag.setInnerText("Суворов");
    //
    //

    System.out.println("66M26zlTS5 :: xml = \n" + root.outerXmlPretty());

    //language=XML
    final String expectedXml = """
      <?xml version="1.0" encoding="UTF-8"?>
      <soap:Top xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Header>
          <c:Client xmlns:c="https://core.mybpm.kz/api-services/bo/">
            <c:Name>Суворов</c:Name>
          </c:Client>
        </soap:Header>
      </soap:Top>
      """;

    assertThat(root.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXml.replaceAll("\\s+", ""));
  }

  @Test
  public void append() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>west</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);


    final XmlTag surname = XmlTag.root("https://core.mybpm.kz/api-services/bo/", "ns2:surname");
    surname.setInnerText("Панфилов");

    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");
    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");

    final XmlTag subTag = xmlTag.gotoTag("soap:Body/m:CreateClient");

    //
    //
    subTag.append(surname);
    //
    //

    System.out.println("wH20lv5xzD :: xmlTag=\n" + xmlTag.outerXmlPretty());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>west</ns2:name>
                  <ns2:surname>Панфилов</ns2:surname>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test
  public void setAttribute() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>west</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag tagName = xmlTag.gotoTag("soap:Body/m:CreateClient/m:name");

    //
    //
    tagName.setAttribute("ns2:activity", "yes");
    //
    //

    System.out.println("F3JoO8g1jw :: xmlTag = \n" + xmlTag.outerXml());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:activity="yes">west</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test
  public void setAttribute__noNamespaces() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <Envelope>
        <Header/>
        <Body>
          <CreateClient>
            <name>west</name>
          </CreateClient>
        </Body>
      </Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    final XmlTag tagName = xmlTag.gotoTag("Body/CreateClient/name");

    //
    //
    tagName.setAttribute("activity", "yes");
    //
    //

    System.out.println("22Iq87OEgS :: xmlTag = \n" + xmlTag.outerXml());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <Envelope >
          <Header/>
          <Body>
              <CreateClient >
                  <name activity="yes">west</name>
              </CreateClient>
          </Body>
      </Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test
  public void children() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <nsx:CreateClient xmlns:nsx="https://core.mybpm.kz/api-services/bo/">
                  <nsx:name>name-SaCAZSpKP1</nsx:name>
                  <nsx:surname>surname-2rW1y1qmXE</nsx:surname>
                  <nsx:patronymic>patronymic-Pm33TQin1M</nsx:patronymic>
                  <nsx:iin>iin-234987567432</nsx:iin>
              </nsx:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag subTag = xmlTag.gotoTag("soap:Body/m:CreateClient");

    //
    //
    final List<XmlTag> children = subTag.children();
    //
    //

    assertThat(children).hasSize(4);

    assertThat(children.get(0).getInnerText()).isEqualTo("name-SaCAZSpKP1");
    assertThat(children.get(1).getInnerText()).isEqualTo("surname-2rW1y1qmXE");
    assertThat(children.get(2).getInnerText()).isEqualTo("patronymic-Pm33TQin1M");
    assertThat(children.get(3).getInnerText()).isEqualTo("iin-234987567432");
  }

  @Test
  public void appendNewTag() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <nsx:CreateClient xmlns:nsx="https://core.mybpm.kz/api-services/bo/" />
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag subTag = xmlTag.gotoTag("soap:Body/m:CreateClient");

    //
    //
    final XmlTag tagName    = subTag.appendNewTag("nsx:name").setAttribute("nsx:Active", "Yes").setInnerText("Иван");
    final XmlTag tagSurname = subTag.appendNewTag("nsx:surname").setAttribute("nsx:Status", "OK").setInnerText("Сидоров");
    //
    //

    System.out.println("9h8SQD4ucJ :: tagName    = " + tagName.outerXml());
    System.out.println("BNj9fT5ORv :: tagSurname = " + tagSurname.outerXml());
    System.out.println("oslWCczK67 :: totalTag = \n" + xmlTag.outerXmlPretty());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <nsx:CreateClient xmlns:nsx="https://core.mybpm.kz/api-services/bo/">
                  <nsx:name nsx:Active="Yes">Иван</nsx:name>
                  <nsx:surname nsx:Status="OK">Сидоров</nsx:surname>
              </nsx:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test
  public void setAttribute__nullValue() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="A-Metis-t">Sharon</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag tagName = xmlTag.gotoTag("soap:Body/m:CreateClient/m:name");

    //
    //
    tagName.setAttribute("ns2:Stone", null);
    //
    //

    System.out.println("Y5pD1zoKKo :: xmlTag = \n" + xmlTag.outerXml());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="">Sharon</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test
  public void removeAttribute() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="A-Metis-t" status="Good">Sharon</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag subTag = xmlTag.gotoTag("soap:Body/m:CreateClient/m:name");

    //
    //
    subTag.removeAttribute("m:Stone").removeAttribute("status");
    //
    //

    System.out.println("Y5pD1zoKKo :: xmlTag = \n" + xmlTag.outerXml());

    //language=XML
    String expectedXmlText = """
      <?xml version="1.0" encoding="UTF-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name>Sharon</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    assertThat(xmlTag.outerXml().replaceAll("\\s+", "")).isEqualTo(expectedXmlText.replaceAll("\\s+", ""));
  }

  @Test(expectedExceptions = ParseXmlError.class)
  public void parseXmlText__leftText() {
    XmlTag.parseXmlText("left");
  }

  @Test(expectedExceptions = ParseXmlError.class)
  public void parseXmlText__null_1() {
    XmlTag.parseXmlText(null);
  }

  @Test(expectedExceptions = ParseXmlNull.class)
  public void parseXmlText__null_2() {
    XmlTag.parseXmlText(null);
  }

  @Test
  public void getAttribute() {
    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
          <SOAP-ENV:Header/>
          <SOAP-ENV:Body>
              <ns2:CreateClient xmlns:ns2="https://core.mybpm.kz/api-services/bo/">
                  <ns2:name ns2:Stone="A-Metis-t" status="Good">Sharon</ns2:name>
              </ns2:CreateClient>
          </SOAP-ENV:Body>
      </SOAP-ENV:Envelope>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("http://schemas.xmlsoap.org/soap/envelope/", "soap");
    xmlTag.addNamespacePrefix("https://core.mybpm.kz/api-services/bo/", "m");

    final XmlTag subTag = xmlTag.gotoTag("soap:Body/m:CreateClient/m:name");

    //
    //
    final String stone  = subTag.getAttribute("m:Stone");
    final String status = subTag.getAttribute("status");
    //
    //

    assertThat(stone).isEqualTo("A-Metis-t");
    assertThat(status).isEqualTo("Good");
  }

  @Test
  public void tagName_noNS() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <asd>
        <client asd="wow"> client 1 </client>
        <status>one</status>
        <client asd="boom"> client 2 </client>
      </asd>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    final XmlTag subTag = xmlTag.gotoTag("asd/status");

    //
    //
    final String tagName = subTag.tagName();
    //
    //

    assertThat(tagName).isEqualTo("status");

  }

  @Test
  public void tagName_withNS_001() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:asd xmlns:SOAP-ENV="https://greetgo.kz/env/" xmlns:SOAP-STATUS="https://greetgo.kz/status/">
        <client asd="wow"> client 1 </client>
        <SOAP-STATUS:status>one</SOAP-STATUS:status>
        <client asd="boom"> client 2 </client>
      </SOAP-ENV:asd>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("https://greetgo.kz/env/", "env");
    xmlTag.addNamespacePrefix("https://greetgo.kz/status/", "s");

    final XmlTag subTag = xmlTag.gotoTag("env:asd/s:status");

    //
    //
    final String tagName = subTag.tagName();
    //
    //

    assertThat(tagName).isEqualTo("s:status");

  }

  @Test
  public void tagName_withNS_002() {

    //language=XML
    String xmlText = """
      <?xml version="1.0" encoding="utf-8"?>
      <SOAP-ENV:asd xmlns:SOAP-ENV="https://greetgo.kz/env/" xmlns:SOAP-STATUS="https://greetgo.kz/status/">
        <client asd="wow"> client 1 </client>
        <SOAP-STATUS:status>one</SOAP-STATUS:status>
        <client asd="boom"> client 2 </client>
      </SOAP-ENV:asd>
      """;

    XmlTag xmlTag = XmlTag.parseXmlText(xmlText);

    xmlTag.addNamespacePrefix("https://greetgo.kz/env/", "env");
    xmlTag.addNamespacePrefix("https://greetgo.kz/status/", "s");

    //
    //
    final String subTagNames = xmlTag.children().stream().map(XmlTag::tagName).collect(Collectors.joining("---"));
    //
    //

    assertThat(subTagNames).isEqualTo("client---s:status---client");
  }
}
