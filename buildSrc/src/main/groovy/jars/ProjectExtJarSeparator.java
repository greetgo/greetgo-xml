package jars;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.jar.Attributes;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import util.FileUtil;

import static java.util.stream.Collectors.joining;
import static util.FileUtil.copyToDir;
import static util.FileUtil.isParent;

public abstract class ProjectExtJarSeparator extends DefaultTask {

  private final List<File> inputFileJars  = new ArrayList<>();
  private final List<File> outputFileJars = new ArrayList<>();

  private final List<Predicate<File>> externFileBlackList = new ArrayList<>();

  private Path   rootDir;
  private Path   distDir;
  private String mainClassName;
  private String jarBaseName;
  private String version;

  private String projectDirName = "project";
  private String externDirName  = "extern";

  @SuppressWarnings("unused")
  public void projectDirName(String name) {
    projectDirName = name;
  }

  @SuppressWarnings("unused")
  public void externDirName(String name) {
    externDirName = name;
  }

  @SuppressWarnings("unused")
  public List<File> outputFileJars() {
    return outputFileJars;
  }

  public List<File> inputFileJars() {
    return inputFileJars;
  }

  public void addFileJar(File file) {
    inputFileJars().add(file);
  }

  public void addToBlackListOfExternJars(Predicate<File> pathJarPredicate) {
    externFileBlackList.add(pathJarPredicate);
  }

  private boolean externFileOk(File externJarPath) {
    for (final Predicate<File> predicate : externFileBlackList) {
      if (predicate.test(externJarPath)) {
        return false;
      }
    }
    return true;
  }

  public void rootDir(File file) {
    rootDir = file.toPath();
  }

  public void distDir(File file) {
    distDir = file.toPath();
  }

  public void mainClass(String mainClassName) {
    this.mainClassName = mainClassName;
  }

  public void jarBaseName(String jarBaseName) {
    this.jarBaseName = jarBaseName;
  }

  public void version(String version) {
    this.version = version;
  }

  boolean isMine(File file) {
    Path localJarsPath = rootDir.resolve("jars");
    return isParent(rootDir, file) && !isParent(localJarsPath, file);
  }

  @TaskAction
  public void execute() throws Exception {

    FileUtil.removeFileOrDir(distDir);

    Path projectJars = distDir.resolve(projectDirName);
    Path externJars  = distDir.resolve(externDirName);

    for (File jar : inputFileJars) {
      if (isMine(jar)) {
        outputFileJars.add(copyToDir(jar, projectJars));
      } else if (externFileOk(jar)) {
        outputFileJars.add(copyToDir(jar, externJars));
      }
    }

    List<Path> projectJarList, externJarList;

    try (Stream<Path> fileStream = Files.list(projectJars)) {
      projectJarList = fileStream.filter(fn -> fn.toFile().getName().toLowerCase().endsWith(".jar"))
                                 .sorted()
                                 .toList();
    }

    try (Stream<Path> fileStream = Files.list(externJars)) {
      externJarList = fileStream.filter(fn -> fn.toFile().getName().toLowerCase().endsWith(".jar"))
                                .sorted()
                                .toList();
    }

    String classPath = Stream.concat(projectJarList.stream(), externJarList.stream())
                             .map(distDir::relativize)
                             .map(Path::toString)
                             .collect(joining(" "));

    Manifest manifest = new Manifest();
    manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
    manifest.getMainAttributes().put(Attributes.Name.CLASS_PATH, classPath);

    if (mainClassName != null && mainClassName.length() > 0) {
      manifest.getMainAttributes().put(Attributes.Name.MAIN_CLASS, mainClassName);
    }

    File mainJarFile = distDir.resolve(jarName()).toFile();
    mainJarFile.getParentFile().mkdirs();
    try (var jarStream = new JarOutputStream(new FileOutputStream(mainJarFile))) {
      jarStream.putNextEntry(new ZipEntry("META-INF/"));
      {
        jarStream.putNextEntry(new ZipEntry("META-INF/MANIFEST.MF"));
        manifest.write(jarStream);
        jarStream.closeEntry();
      }
      jarStream.closeEntry();
    }
  }

  private String jarName() {
    return jarBaseName + (version == null ? "" : '-' + version) + ".jar";
  }

}
