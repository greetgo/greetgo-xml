package show;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.TaskAction;

public class ShowTestRuntimeJars extends DefaultTask {

  private FileCollection classpath;

  public ShowTestRuntimeJars setClasspath(FileCollection classpath) {
    this.classpath = classpath;
    return this;
  }

  @TaskAction
  public void execute() throws Exception {

    if (classpath == null) {
      throw new RuntimeException("ZJ1BM6s2Pl :: classpath is not defined. Defined it please");
    }

    File outFile1 = getProject().getBuildDir().toPath().resolve("list_classpath.txt").toFile();
    File outFile2 = getProject().getBuildDir().toPath().resolve("list_classpath_with_classes.txt").toFile();
    outFile1.getParentFile().mkdirs();
    outFile2.getParentFile().mkdirs();

    List<File> jarList = new ArrayList<>();

    for (final File file : classpath) {
      if (!file.getName().endsWith(".jar")) {
        continue;
      }
      jarList.add(file);
    }

    jarList.sort(Comparator.comparing(File::getName));

    int nameLength = jarList.stream().mapToInt(f -> f.getName().length()).max().orElse(1);

    try (
      FileOutputStream outputStream1 = new FileOutputStream(outFile1);
      PrintStream out1 = new PrintStream(outputStream1, false, StandardCharsets.UTF_8);
      FileOutputStream outputStream2 = new FileOutputStream(outFile2);
      PrintStream out2 = new PrintStream(outputStream2, false, StandardCharsets.UTF_8)
    ) {

      for (final File jarFile : jarList) {
        if (!jarFile.getName().endsWith(".jar")) {
          continue;
        }
        out1.println(toLenEnd(jarFile.getName(), nameLength) + "  :  " + jarFile);
        out2.println(jarFile);
        out2.println();
        for (final String className : listClassNames(jarFile)) {
          out2.println(jarFile.getName() + " : " + className);
        }
        out2.println();
        out2.println("#############################################################################################################################");
        out2.println("#############################################################################################################################");
        out2.println("#############################################################################################################################");
        out2.println();
      }

    }


  }

  public static String toLenEnd(String s, int len) {
    StringBuilder sb = new StringBuilder(len);
    sb.append(s);
    while (sb.length() < len) {
      sb.append(' ');
    }
    return sb.toString();
  }

  private List<String> listClassNames(File jarFile) throws Exception {
    List<String> ret = new ArrayList<>();
    try (var fileInputStream = new FileInputStream(jarFile);
         var zip = new ZipInputStream(fileInputStream);) {
      while (true) {
        ZipEntry entry = zip.getNextEntry();
        if (entry == null) {
          ret.sort(Comparator.comparing(s -> s));
          return ret;
        }

        String name = entry.getName();
        zip.closeEntry();

        String ext = ".class";

        if (name.endsWith(ext)) {
          String classFileName = name.substring(0, name.length() - ext.length());
          ret.add(classFileName.replace('/', '.'));
        }
      }
    }
  }

}
