package plugins

import plugins.model.MavenUploadPluginExtension
import org.gradle.api.DefaultTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.javadoc.Javadoc
import org.gradle.configuration.project.ProjectConfigurationActionContainer
import org.gradle.plugins.signing.Sign

import javax.inject.Inject

class GreetgoPublisher implements Plugin<Project> {
  public static final String UPLOAD_INFO = "uploadInfo"

  private final ProjectConfigurationActionContainer configurationActionContainer

  @Inject
  GreetgoPublisher(ProjectConfigurationActionContainer configurationActionContainer) {
    this.configurationActionContainer = configurationActionContainer
  }

  @Override
  void apply(Project project) {

    def publishToMavenRepoExists = true

    if (false
      || Env.sonatypeAccountId() == null
      || Env.sonatypeAccountPassword() == null
    ) {
      LocalUtil.printNoSonatypeCredentials()
      publishToMavenRepoExists = false
    }

    def publishToGgRepoExists = true

    if (Env.ggRepoUrl() == null) {
      LocalUtil.printNoRepoError()
      publishToGgRepoExists = false
    }

    MavenUploadPluginExtension ext = project.extensions.create(UPLOAD_INFO, MavenUploadPluginExtension)

    LocalUtil.registerSourcesJavadocJarTasks(project)

    def useSigning = Env.signGpgKeyId() != null

    if (useSigning) {
      project.pluginManager.apply("signing")
    }

    project.pluginManager.apply("java")
    project.pluginManager.apply("maven-publish")

    project.gradle.taskGraph.whenReady { taskGraph ->
      if (taskGraph.allTasks.any { it instanceof Sign }) {
        project.gradle.allprojects { it.ext."signing.keyId" = Env.signGpgKeyId() }
        project.gradle.allprojects { it.ext."signing.secretKeyRingFile" = Env.signGpgKeyLocation() }
        project.gradle.allprojects { it.ext."signing.password" = Env.signGpgKeyPassword() }
      }
    }

    configurationActionContainer.add {

      if (ext.description == null) {
        LocalUtil.printExtUploadToMavenCentral()
        throw new RuntimeException("xYycFVkxHq :: " + UPLOAD_INFO + ".description == null")
      }
      if (ext.url == null) {
        LocalUtil.printExtUploadToMavenCentral()
        throw new RuntimeException("45I8ggT9uA :: " + UPLOAD_INFO + ".url == null")
      }

      project.publishing {
        publications {
          mainJava(MavenPublication) {
            from project.components.java

            pom {
              //noinspection GroovyAssignabilityCheck
              packaging = "jar"
              name.set(project.name)

              description.set(ext.description)
              url.set(ext.url)

              scm {
                connection.set(ext.scm.url)
                developerConnection.set(ext.scm.connection)
                url.set(ext.scm.devConnection)
              }

              licenses {
                license {
                  name.set("The Apache License, Version 2.0")
                  url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                }
              }

              developers {
                for (int i = 0; i < ext.developers.size(); i++) {
                  def d = ext.developers[i]
                  developer {
                    id.set(d.id)
                    name.set(d.name)
                    email.set(d.email)
                  }
                }
              }
            }
          }
        }

        repositories {

          if (publishToMavenRepoExists) {

            maven {
              name = "mavenCentral"
              def releasesUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
              def snapshotsUrl = uri("https://oss.sonatype.org/content/repositories/snapshots/")
              url = version.toString().endsWith("SNAPSHOT") ? snapshotsUrl : releasesUrl
              credentials {
                username = Env.sonatypeAccountId()
                password = Env.sonatypeAccountPassword()
              }
            }

          }

          if (publishToGgRepoExists) {

            def ggRepoUrl = Env.ggRepoUrl() ?: 'http://192.168.17.151:8081/repository/maven-releases/'

            maven {
              allowInsecureProtocol = true
              name = "ggRepo"
              url = uri(ggRepoUrl)

              def userName = Env.ggRepoUsername()
              def userPassword = Env.ggRepoPassword()

              if (userName && userPassword) {
                credentials {
                  username = userName
                  password = userPassword
                  allowInsecureProtocol = true
                }
              }
            }

          }
        }

      }

      if (useSigning) {
        project.signing {
          sign publishing.publications['mainJava']
        }
      }

      project.java {
        withJavadocJar()
        withSourcesJar()
      }

      project.tasks.withType(Javadoc) {
        //noinspection GroovyAssignabilityCheck
        failOnError false
        //noinspection SpellCheckingInspection
        options.addStringOption('Xdoclint:none', '-quiet')
        options.addStringOption('encoding', 'UTF-8')
        options.addStringOption('charSet', 'UTF-8')
      }

      if (publishToGgRepoExists) {
        project.tasks.create("upload_to_gg_repo", DefaultTask.class) {
          group = "upload"
          dependsOn "publishMainJavaPublicationToGgRepoRepository"
        }
      }

      if (publishToMavenRepoExists) {
        project.tasks.create("upload_to_maven_repo", DefaultTask.class) {
          group = "upload"
          dependsOn "publishMainJavaPublicationToMavenCentralRepository"
        }
      }

      project.tasks.create("upload_to_local", DefaultTask.class) {
        group = "upload"
        dependsOn "publishMainJavaPublicationToMavenLocal"
      }

    }

  }
}
